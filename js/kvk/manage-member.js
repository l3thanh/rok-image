$(document).ready(function ($) {
    /**
     * Prepare
     */
    function preparehtml(total) {
        var html = "";
        for (var i = 0; i < total; i++) {
            html += "<tr>";
            html += '<th scope="row">';
            html += i + 1;
            html += "</th>";
            html += "<td id='field_id_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td id='field_name_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td id='field_kt4_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td id='field_kt5_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td id='field_death_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td id='field_rss_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td class='field_image' id='field_image1_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "<td class='field_image' id='field_image2_" + i + "'>";
            html += "pending";
            html += "</td>";
            html += "</tr>";
        }
        $("#show_information").html(html);
    }

    /**
     * render html
     */
    function renderHtml(data, index) {
        $("#field_id_" + index).html(data.id ? data.id : "ErrorID");
        $("#field_name_" + index).html(data.name ? data.name : "...");
        $("#field_kt4_" + index).html(data.kill_t4 ? data.kill_t4 : "ErrorT4");
        $("#field_kt5_" + index).html(data.kill_t5 ? data.kill_t5 : "ErrorT5");
        $("#field_death_" + index).html(data.death ? data.death : "ErrorDead");
        $("#field_rss_" + index).html(data.rss ? data.rss : "ErrorRss");
        $("#field_image1_" + index).html(
            data.image_1
                ? '<a href="' +
                      data.image_1 +
                      '" target="_blank"><img src="' +
                      data.image_1 +
                      '" alt="image1" class="img-thumbnail" id="image_modal"></img></a>'
                : "Error"
        );
        $("#field_image2_" + index).html(
            data.image_2
                ? '<a href="' +
                      data.image_2 +
                      '" target="_blank"><img src="' +
                      data.image_2 +
                      '" alt="image2" class="img-thumbnail" id="image_modal"></img></a>'
                : "Error"
        );
    }

    //Change name js when upload
    $("#images").change(function () {
        var images = $("#images")[0].files;
        if (images.length == 0) {
            $("#images_label").text("Chọn file đi!");
        } else {
            var text = images.length + " files:";
            // $.each(images, function (key, image) {
            //     if (key < 5) {
            //         text += " " + image.name;
            //         if (key !== images.length - 1) {
            //             text += ",";
            //         }
            //     } else {
            //         text += " ...";
            //         return false;
            //     }
            // });
            $("#images_label").text(text);
        }
    });

    //Array chunk
    function chunkArray(myArray, chunk_size) {
        var results = [];
        while (myArray.length) {
            results.push(myArray.splice(0, chunk_size));
        }

        return results;
    }

    //Event submit
    $("#submit").click(function (e) {
        e.preventDefault();
        //Enter api key first
        if (!$("#api_key").val()) {
            $("#api_key").addClass("is-invalid");
            $("#invalid_feedback_api_key").text("Nhập Api Key vô pa!!!");
        } else {
            if ($("#api_key").hasClass("is-invalid")) {
                $("#api_key").removeClass("is-invalid");
            }
            //images
            var images = $.makeArray($("#images")[0].files);
            //Validate images
            if (images.length == 0 || images.length % 2 == 1) {
                $("#images").addClass("is-invalid");
                $("#invalid_feedback_images").text(
                    "Chưa nhập image hoặc image bị thiếu"
                );
            } else {
                if ($("#images").hasClass("is-invalid")) {
                    $("#images").removeClass("is-invalid");
                }
                // Show results
                buttonShowHide();
                $("#show-table").show();
                preparehtml(images.length / 2);
                //Get file by file
                var arrImages = chunkArray(images, 2);
                //Run ajax
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });
                $.each(arrImages, function (key, image) {
                    //Config value image
                    var formData = new FormData();
                    formData.append("image_1", image[0]);
                    formData.append("image_2", image[1]);
                    formData.append("api_key", $("#api_key").val());

                    $.ajax({
                        type: "POST",
                        url: "ajaxkvkmember",
                        data: formData,
                        dataType: "JSON",
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (response) {
                            renderHtml(response.data, key);
                        },
                        error: function (responseError) {},
                    });
                });
            }
        }
    });

    /**
     * All button event
     */
    $("#show_image").click(function (e) {
        $(".field_image").toggle();
    });

    $("#clear_form").click(function (e) {
        if (confirm("Chắc chắn clear hết dữ liệu ?")) {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                type: "POST",
                url: "ajaxremoveimages",
                success: function () {
                    location.reload();
                },
            });
        }
    });

    $("#remove_image").click(function (e) {
        if (confirm("Chắc chắn xóa hết ảnh")) {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
            });
            $.ajax({
                type: "POST",
                url: "ajaxremoveimages",
                success: function () {
                    alert("Done remove");
                },
            });
        }
    });

    /**
     * Check show button
     */
    function buttonShowHide() {
        $("#submit").hide();
        $("#clear_form").show();
        $("#show_image").show();
        $("#remove_image").show();
    }
});
