@extends('layouts.app')

@section('title', 'Manage member')

@section('sidebar')
@parent
@endsection

@section('content')
{{ ini_get('post_max_size') }}
{{ ini_get('upload_max_filesize') }}
<form action="" method="post" enctype="multipart/form-data" class="pb-3" id="kvk_before">
    @csrf
    {{-- Api key --}}
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Api key: </label>
        <div class="col-sm-7 nopadding">
            <input type="text" class="form-control" id="api_key" name="api_key" placeholder="7bfa219a4988957" required
                value="7bfa219a4988957">
            <div class="invalid-feedback" id="invalid_feedback_api_key"></div>
        </div>
        <div class="col-sm-3">
            <a href="#" class="d-sm-none" data-toggle="modal" data-target="#guild_Api_key">Cách lấy api-key</a>
            <button type="button" class="btn btn-info d-none d-sm-block" data-toggle="modal"
                data-target="#guild_Api_key">Hướng dẫn</button>
        </div>
    </div>

    {{-- Images --}}
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Images:</label>
        <div class="col-sm-7">
            <input type="file" class="custom-file-input" id="images" name="images[]" multiple required>
            <label class="custom-file-label" id="images_label">Chọn nhiều file ...</label>
            <div class="invalid-feedback" id="invalid_feedback_images"></div>
        </div>
        <div class="col-sm-3">
            <a href="#" class="d-sm-none" data-toggle="modal" data-target="#guild_upload_image">Please take pictures as
                instructed</a>
            <button type="button" class="btn btn-info d-none d-sm-block" data-toggle="modal"
                data-target="#guild_upload_image">Hướng dẫn</button>
        </div>
    </div>
    <div class="d-flex justify-content-around">
        <button type="submit" id="submit" class="btn btn-primary">Run script</button>
        <button type="button" id="clear_form" class="btn btn-info hidden">Clear</button>
        <button type="button" id="show_image" class="btn btn-info hidden">Show/hide image</button>
        <button type="button" id="remove_image" class="btn btn-danger hidden">Remove all images</button>
    </div>
</form>

<!--  -->
<table class="table table-striped" id="show-table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Kill t4</th>
            <th scope="col">Kill t5</th>
            <th scope="col">Death</th>
            <th scope="col">Ressource</th>
            <th scope="col">Image 1</th>
            <th scope="col">Image 2</th>
        </tr>
    </thead>
    <!--  -->
    <tbody id="show_information">
    </tbody>
</table>

{{-- Modal hướng dẫn upload image --}}
<div class="modal fade bd-example-modal-lg" id="guild_upload_image" tabindex="-1" role="dialog"
    aria-labelledby="guildUploadImage" aria-hidden="true">
    <div class="modal-lg modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="guildUploadImage">Hướng dẫn cách thêm Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold">Tải file mẫu tại đây : <a
                        href="https://drive.google.com/file/d/1cRJCTYMl1aPp_Uai6QYTDPZ2c7r_AYWy/view?usp=drive_open"
                        target="_blank"> File test </a></p>
                <p class="font-weight-bold"><u>Cách sử dụng:</u></p>
                <p class="font-weight-normal">
                    <strong>I.</strong> Chụp hình theo mẫu sau, bắc buộc người chụp phải <strong>tiếng anh</strong>:
                    <span class="border">
                        <img src="{{ url('/img/test/1.jpg') }}" class="rounded mx-auto d-block border" alt="image1"
                            width="512px" height="288px">
                    </span>
                    <span class="border">
                        <img src="{{ url('/img/test/2.jpg') }}" class="rounded mx-auto d-block border" alt="image2"
                            width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>II.</strong> 2 hình ảnh của cùng 1 user cần để kế bên nhau.
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_1.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>III.</strong> Vào trang tool và click vào Browse
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_2.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>IV.</strong> Chọn hết tất cả ảnh đã chuẩn bị ở bước II sau đó ấn open
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_3.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>V.</strong> Ấn Run script, ngồi chờ kết quả. Kết quả có 1 số ít người bị error, tạm thời tự
                    nhập tay bằng image bên cạnh và báo lại giúp mình.
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_4.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>VI.</strong> Lấy dữ liệu đưa vào file excel.
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_5.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                    Paste vào file excel, nó sẽ hiển thị đúng từng cột.
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_6.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>VII.</strong> Sau khi xong hết nhớ ấn xóa image để server nhẹ bớt.
                    <span class="border">
                        <img src="{{ url('/img/guide_upload_7.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-xl" id="guild_Api_key" tabindex="-1" role="dialog" aria-labelledby="guildApiKey"
    aria-hidden="true">
    <div class="modal-xl modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="guildApiKey">Cách để lấy api free</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="image_modal_show">
                <p class="font-weight-normal">Mua hosting cùi nó không cho tải thử viện, nên đổi qua sài api free. Mỗi
                    ngày nó cho scan 500 ảnh thôi, ae tự lấy thêm nha.</p>
                <p class="font-weight-normal">2 key sài tạm: 43958ca08088957, 7bfa219a4988957</p>
                <p class="font-weight-normal">Vào thời gian cần gấp và nhiều thì tự lấy key theo cách sau nha</p>
                <p class="font-weight-normal">
                    <strong>I.</strong> Vào link này: <a href="https://ocr.space/ocrapi" target="_blank"> Link lấy Api
                    </a>
                </p>
                <p class="font-weight-normal">
                    <strong>II.</strong> Ấn zô đây
                    <span class="border">
                        <img src="{{ url('/img/guide_api_key_1.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
                <p class="font-weight-normal">
                    <strong>III.</strong> Lười viết quá. Cứ đăng ký và làm theo nó bình thường cho đến khi nó gửi api về
                    gmail như này:
                    <span class="border">
                        <img src="{{ url('/img/guide_api_key_2.jpg') }}" class="rounded mx-auto d-block border"
                            alt="image2" width="512px" height="288px">
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ url('/js/kvk/manage-member.js') }}"></script>
@endpush