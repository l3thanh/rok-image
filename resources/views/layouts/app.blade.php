<html>

<head>
    <title>App Name - @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/rok.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ url('/js/bootstrap.min.js')}}"></script>
</head>

<body>
    <div class="container rounded">
        @section('sidebar')
        <nav class="navbar navbar-light">
            <img src="{{ url('/img/logo.png') }}" class="rounded mx-auto d-block w-25 p-3" alt="Logo">
        </nav>
        @show

        @yield('content')
    </div>
    @stack('scripts')
</body>

</html>