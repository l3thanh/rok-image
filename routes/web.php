<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Kvk\KvkMemberController;

Route::get('/', function () {
    return view('welcome');
});

//Kvk
Route::get('kvk/manage-member', [KvkMemberController::class, 'show']);
//Route::post('kvk/manage-member', [KvkMemberController::class, 'show']);

Route::post('kvk/ajaxkvkmember', [KvkMemberController::class, 'ajaxKvkMember']);
Route::post('kvk/ajaxremoveimages', [KvkMemberController::class, 'ajaxRemoveImages']);