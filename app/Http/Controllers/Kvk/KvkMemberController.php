<?php
namespace App\Http\Controllers\Kvk;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;

class KvkMemberController extends Controller
{
    // 43958ca08088957
    // 7bfa219a4988957
    public $passCutString = 'Ksb345#bs';

    /**
     * Show view kvk before
     *
     * @return View
     */
    public function show()
    {
        return view('kvk.manage-member');
    }

    /**
     *
     */
    private function handleImage1($arrResults) {
        //Execute
        $results = [
            'id' => 0,
            'kill_t4' => 0,
            'kill_t5' => 0,
        ];
        
        $checkT4Exists = false;
        foreach($arrResults as $value) {
            // var_dump($value);
            /**
             * Id: 
             * T4: 
             * T5: 
             */
            $minTop = $value['MinTop'];
            if( $minTop > 275 && $minTop < 296 ) {
                foreach($value['Words'] as $words) {
                    $left = $words['Left'];
                    if( $left > 932 && $left < 952 ) {
                        $id = $value['LineText'];
                        $position = strpos($id, 'D');
                        if($position == false) {
                            $id = substr($id, $position);
                        } 
                        $id = preg_replace('/[^0-9]/', '', $id);
                        if(strlen($id) > 8) {
                            $results['id'] = substr($id, 1);
                        } else {
                            $results['id'] = $id;
                        }
                    }
                }
            } elseif ( $minTop > 605 && $minTop < 625 ) { //Get T4
                foreach($value['Words'] as $words) {
                    $left = $words['Left'];
                    // var_dump($value);
                    if( $left > 1300 && $left < 1350 ) {
                        // var_dump('done');
                        $results['kill_t4'] = $words['WordText'];
                        $checkT4Exists = true;
                    } elseif ($left > 1000 && $left < 1500 && !$checkT4Exists) {
                        // var_dump('fail');
                        $killT4 = explode(",", $words['WordText']);
                        foreach($killT4 as $key => $value) {
                            if (strlen($value) < 4) {
                                unset($killT4[$key]);
                            } else {
                                $killT4[$key] = substr($value, 3);
                                break;
                            }
                        }
                        $results['kill_t4'] = implode(',', $killT4);
                    }
                }
            } elseif ( $minTop > 656 && $minTop < 668) { //Get T5
                foreach($value['Words'] as $words) {
                    $left = $words['Left'];
                    if( $left > 1155 && $left < 1168 ) {
                        $results['kill_t5'] = $words['WordText'];
                    }
                }
            }
        }
        // var_dump($results);
        // die;

        return $results;
    }

    /**
     *
     */
    private function handleImage2($arrResults) {
        //Execute
        $results = [
            'name' => '',
            'death' => 0,
            'rss' => 0,//power
        ];
        foreach($arrResults as $value) {
            // var_dump($value);
            /**
             * Name  : 
             * Death : 
             * Power : 
             */
            $minTop = $value['MinTop'];
            if ( $minTop > 534 && $minTop < 557 ) {
                foreach($value['Words'] as $words) {
                    $left = $words['Left'];
                    if( $left > 1420 && $left < 1500 ) {
                        $results['death'] = $words['WordText'];
                    }
                }
            } elseif ( $minTop > 166 && $minTop < 185) {
                foreach($value['Words'] as $words) {
                    $left = $words['Left'];
                    if( $left > 895 && $left < 1000 ) {
                        $results['rss'] .= $words['WordText'];
                    }
                }
            }
        }
        $results['name'] = 'NULL';
        $results['rss'] = preg_replace('/[^0-9,]/', '', $results['rss']);
        // die;

        return $results;
    }

    /**
     * 
     */
    public function ajaxRemoveImages(Request $request) {
        $images = $request->session()->get('image');
        if(!empty($images)) {
            foreach($images as $value) {
                Storage::delete($value);
            }
            session_unset();
        }

        return response()->json( ['status' => 'success'], 200);
    }

    /**
     * Ajax kvk member
     */
    public function ajaxKvkMember(Request $request) {
        try {
            $data = $request->all();
            //Validate data

            //Get data
            $apiKey = $data['api_key'];
            $images1 = $data['image_1'];
            $images2 = $data['image_2'];

            $imageFirst = $images1;
            // Save JPEG image
            $name = uniqid() . '.jpg';
            if(!Storage::put($name, file_get_contents($imageFirst))) {
                return false;
            }
            // Get Url image
            $url = storage_path() . "/app/" . $name;
            //Call Api
            $callApi = Http::withOptions([
                'verify' => false,
            ])
            ->attach('url', file_get_contents($url), $name)
            ->post('https://api.ocr.space/parse/image', [
                'apikey' => $apiKey,
                'scale' => 'false',
                'isTable' => 'true',
                'OCREngine' => 2,
            ])->json();
            $results1 = $this->handleImage1($callApi['ParsedResults'][0]['TextOverlay']['Lines']);
            $results1['image_1'] = URL::to('/storage/app/' . $name);
            //Session
            $request->session()->push('image', $name);

            /**
             * Image second : kill t4, kill t5, death, resource
             */
            $imageSecond= $images2;
            $name = uniqid() . '.jpg';
            if(!Storage::put($name, file_get_contents($imageSecond))) {
                return false;
            }
            // Get Url image
            $url = storage_path() . "/app/" . $name;
            //Call Api
            $callApi = Http::withOptions([
                'verify' => false,
            ])
            ->attach('url', file_get_contents($url), $name)
            ->post('https://api.ocr.space/parse/image', [
                'apikey' => $apiKey,
                'scale' => 'false',
                'isTable' => 'true',
                'OCREngine' => 2,
            ])->json();
            $results2 = $this->handleImage2($callApi['ParsedResults'][0]['TextOverlay']['Lines']);
            $results2['image_2'] =  URL::to('/storage/app/' . $name);
            $request->session()->push('image', $name);

            return response()->json( ['status' => 'success', 'data' => array_merge($results1, $results2)], 200);
        } catch (Exception $e) {
            
            return response()->json( ['status' => 'error123', 'error' => $e->getMessage()], 200);
        }
        
    }
}